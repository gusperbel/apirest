package com.indigo.apiRest.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ApiResponse {
	public ApiResponse(){    
    }
    public ApiResponse(int id, String content){
        this.id = id;
        this.content = content;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setContent(String content) {
        this.content = content;
    }
    private int id;
    private String content;
    
    public int getId() {
        return id;
    }

    public String getContent() {
        return content;
    }
    public String toString() {
    	return "{\"id\" : \""+id+"\", \"content\" : \"Hello, "+content+"!\"}";
    }
}
