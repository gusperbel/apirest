package com.indigo.apiRest.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import com.indigo.apiRest.modelo.ApiResponse;

@Path("/greeting")
public class ServiceGreeting {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getContent(@QueryParam("name") String name){
    	ApiResponse resp = new ApiResponse(1,name);    	
    	return resp.toString();
    }
}